/**
  ******************************************************************************
  * File Name          : main.c
  * Description        : Main program body
  ******************************************************************************
  *
  * COPYRIGHT(c) 2017 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f1xx_hal.h"
#include "adc.h"
#include "dma.h"
#include "i2c.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* USER CODE BEGIN Includes */
#include <stdio.h>
#include <string.h>
#include <math.h>
/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
#define BAT_MAX 84
#define BAT_MIN 60

#define HIST 10

#define EEPROM_ADDR 	0xA0

#define LED1	(1<<0)
#define LED2	(1<<1)
#define LED3	(1<<2)
#define LED4	(1<<3)
#define LED5	(1<<4)
#define LED6	(1<<5)
#define LED7	(1<<6)
#define LED8	(1<<7)

uint16_t silnik_prawy = 0, silnik_lewy = 0;
uint16_t adc[10] = {0};
uint16_t prog_kalib = 0;

volatile uint8_t czyszczenie = 0, bat_stan = 0, sterowanie = 0, police_light = 0;

volatile uint8_t keys_flag = 0, jazda_flag = 0, test_flag = 0;
uint8_t leds_status, procent, key[3] = {0}, jazda = 0, test = 0, connected = 0;
uint8_t czujniki[8] = {0};

/* ZMIENNE DO BLUETOOTHA */
uint8_t Received;
uint8_t rx_size = 0, rx_buff[32], tx_buff[32];
/* ZMIENNE DO JAZDY */
uint16_t kp = 15, kd = 0, vzad = 100;
uint8_t prev_err = 0, przestrzelony = 0;
int8_t wagi[8] = {-8, -6, -4, -2, 2, 4, 6, 8};
int blad, pop_blad;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void Error_Handler(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/
void silniki(int16_t lewy, int16_t prawy);
int licz_blad(void);
int reg_pd(void);

void key_pressed(const uint8_t key);
uint8_t sprawdz_baterie(void);
void leds(const uint8_t led);
void kalibracja(void);
void odczyt_czujnikow(void);
void zapisz_nastawy(void);
void wczytaj_nastawy(void);
void rx_bt_ready(void);

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

int main(void)
{

  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* Configure the system clock */
  SystemClock_Config();

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_ADC1_Init();
  MX_I2C1_Init();
  MX_UART4_Init();
  MX_TIM3_Init();
  MX_TIM1_Init();
  MX_TIM7_Init();
  MX_TIM6_Init();

  /* USER CODE BEGIN 2 */
  HAL_TIM_PWM_Start_DMA(&htim1, TIM_CHANNEL_2, (uint32_t *) &silnik_prawy, 1);
  HAL_TIM_PWM_Start_DMA(&htim3, TIM_CHANNEL_1, (uint32_t *) &silnik_lewy, 1);
  HAL_ADC_Start_DMA(&hadc1, (uint32_t*) &adc, 10);
  HAL_TIM_Base_Start_IT(&htim6);
  HAL_TIM_Base_Start_IT(&htim7);

  HAL_GPIO_WritePin(LED8_GPIO_Port, LED8_Pin, 1);
  HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, 1);
  HAL_Delay(50);
  HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, 1);
  HAL_GPIO_WritePin(LED7_GPIO_Port, LED7_Pin, 1);
  HAL_Delay(50);
  HAL_GPIO_WritePin(LED3_GPIO_Port, LED3_Pin, 1);
  HAL_GPIO_WritePin(LED6_GPIO_Port, LED6_Pin, 1);
  HAL_Delay(50);
  HAL_GPIO_WritePin(LED4_GPIO_Port, LED4_Pin, 1);
  HAL_GPIO_WritePin(LED5_GPIO_Port, LED5_Pin, 1);
  HAL_Delay(250);
  HAL_GPIO_WritePin(LED4_GPIO_Port, LED4_Pin, 0);
  HAL_GPIO_WritePin(LED5_GPIO_Port, LED5_Pin, 0);
  HAL_Delay(50);
  HAL_GPIO_WritePin(LED3_GPIO_Port, LED3_Pin, 0);
  HAL_GPIO_WritePin(LED6_GPIO_Port, LED6_Pin, 0);
  HAL_Delay(50);
  HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, 0);
  HAL_GPIO_WritePin(LED7_GPIO_Port, LED7_Pin, 0);

  leds_status = (LED1|LED8);
  wczytaj_nastawy();
  procent = sprawdz_baterie();
  kalibracja();

  HAL_UART_Receive_IT(&huart4, &Received, 1);
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
	  if(!jazda)
	  {
		  if(czyszczenie)
			  silniki(300, 300);
		  else if(!sterowanie)
			  silniki(0, 0);

		  if(connected && !HAL_GPIO_ReadPin(BT_STATE_GPIO_Port, BT_STATE_Pin))
		  {
			  connected = 0;
			  jazda = 0;
			  test = 0;
			  sterowanie = 0;
		  }

		  if(sterowanie)
		  {
			  leds_status = (LED2|LED7);
		  }
		  else if(test)
		  {
			  if(test_flag)
			  {
				  test_flag = 0;
				  uint8_t i;
				  odczyt_czujnikow();
				  leds_status = 0;
				  for(i = 0; i < 8; i++)
				  {
					  if(czujniki[i])
						  leds_status |= (1<<i);
				  }
				  uint8_t len = sprintf((char*)tx_buff, "TS%d%d%d%d%d%d%d%d%3d\n",czujniki[0],czujniki[1],czujniki[2],czujniki[3],czujniki[4],czujniki[5],czujniki[6],czujniki[7],sprawdz_baterie());
				  HAL_UART_Transmit_IT(&huart4, tx_buff, len);
			  }
		  }
		  else if(bat_stan)
		  {
			  uint8_t i;
			  procent = sprawdz_baterie();
			  for(i = 0; i < 8; i++)
			  {
				if(procent > 100/8*i)
					leds_status |= (1<<i);
			  }
		  }
		  else
		  {
			  leds_status = (LED1|LED8);
		  }
	  }
	  else if(jazda_flag)
	  {
		  jazda_flag = 0;
		  /* ODCZYT LINII */
		  odczyt_czujnikow();
		  /* LICZ UCHYB */
		  blad = licz_blad();
		  int regulacja = reg_pd();
		  /* STERUJ SILNIKAMI */
		  silniki(vzad - regulacja, vzad + regulacja);
		  /* OGARNIJ DIODY */
		  /* POLICYJNE LED */
		  if(police_light < 6 && police_light%2 == 1) leds_status = LED4;
		  else if(police_light > 8 && police_light%2 == 1) leds_status = LED5;
		  else leds_status = 0;
		  /* ZWYKLE LED */
		  //leds_status = (LED4|LED5);
	  }
	  if(keys_flag)
	  {
		  leds(leds_status);
		  keys_flag = 0;
		  if(((key[0] = (key[0]<<1) | HAL_GPIO_ReadPin(KEY1_GPIO_Port, KEY1_Pin)) & 0x0F)  == 0x08) key_pressed(1);
		  if(((key[1] = (key[1]<<1) | HAL_GPIO_ReadPin(KEY2_GPIO_Port, KEY2_Pin)) & 0x0F)  == 0x08) key_pressed(2);
		  if(((key[2] = (key[2]<<1) | HAL_GPIO_ReadPin(KEY3_GPIO_Port, KEY3_Pin)) & 0x0F)  == 0x08) key_pressed(3);
	  }
  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */

}

/** System Clock Configuration
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.Prediv1Source = RCC_PREDIV1_SOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL6;
  RCC_OscInitStruct.PLL2.PLL2State = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_ADC;
  PeriphClkInit.AdcClockSelection = RCC_ADCPCLK2_DIV6;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

    /**Configure the Systick interrupt time 
    */
  __HAL_RCC_PLLI2S_ENABLE();

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* USER CODE BEGIN 4 */
int reg_pd(void)
{
    int rozniczka = blad - pop_blad;
    pop_blad = blad;
    return kp*blad + kd*rozniczka;
}

int licz_blad(void)
{
    int err = 0;
    int ilosc = 0;

    for(int i = 0; i < 8; i++)
    {
    	err += czujniki[i]*wagi[i];
        ilosc += czujniki[i];
    }

    if(ilosc)
    {
        err /= ilosc;
        prev_err = err;
    }
    else
    {
        if(prev_err < wagi[2])
        {
            err = wagi[1];
            przestrzelony = 1;
        }
        else if(prev_err > wagi[5])
        {
            err = wagi[6];
            przestrzelony = 2;
        }
        else
            err = 0;
    }

    if(przestrzelony == 1 && err >= 0) /* Jesli po utracie lini ja przecial */
        przestrzelony = 0;
    else if(przestrzelony == 2 && err <= 0) /* Jesli po utracie lini ja przecial */
        przestrzelony = 0;

    return err;
}

void silniki(int16_t lewy, int16_t prawy)
{
	HAL_GPIO_WritePin(EN1A_GPIO_Port, EN1A_Pin, prawy>=0?1:0);
	HAL_GPIO_WritePin(EN1B_GPIO_Port, EN1B_Pin, prawy>=0?0:1);
	if(prawy < 0) prawy*= -1;
	if(prawy > 1000) prawy = 1001;
	silnik_prawy = prawy;

	HAL_GPIO_WritePin(EN2A_GPIO_Port, EN2A_Pin, lewy>=0?1:0);
	HAL_GPIO_WritePin(EN2B_GPIO_Port, EN2B_Pin, lewy>=0?0:1);
	if(lewy < 0) lewy *= -1;
	if(lewy > 1000) lewy = 1001;
	silnik_lewy = lewy;
}

uint8_t sprawdz_baterie(void)
{
	double vbat;
	vbat = 1.2*adc[0]/adc[9];
	vbat *= 147;
	vbat /= 4.7;
	vbat += 4;
	uint8_t proc = (vbat-BAT_MIN)*100/(BAT_MAX-BAT_MIN);
	if(proc > 100) proc = 100;
	else if(proc < 0) proc = 0;
	//uint8_t proc = vbat;///BAT_MAX;
	return proc;
}

void leds(const uint8_t led)
{
	HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, led&(1<<0));
	HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, led&(1<<1));
	HAL_GPIO_WritePin(LED3_GPIO_Port, LED3_Pin, led&(1<<2));
	HAL_GPIO_WritePin(LED4_GPIO_Port, LED4_Pin, led&(1<<3));
	HAL_GPIO_WritePin(LED5_GPIO_Port, LED5_Pin, led&(1<<4));
	HAL_GPIO_WritePin(LED6_GPIO_Port, LED6_Pin, led&(1<<5));
	HAL_GPIO_WritePin(LED7_GPIO_Port, LED7_Pin, led&(1<<6));
	HAL_GPIO_WritePin(LED8_GPIO_Port, LED8_Pin, led&(1<<7));
}

void key_pressed(const uint8_t key)
{
	if(!jazda)
	{
		if(key == 1)
		{
			procent = sprawdz_baterie();
			bat_stan = 100;
			leds_status = 0;
		}
		else if(key == 2)
		{

			if(jazda) jazda = 0;
			else
			{
				kalibracja();
				jazda = 1;
			}
		}
		else if(key == 3)
		{
			if(test) test = 0;
			else
			{
				kalibracja();
				test = 1;
			}
		}
	}
	else
		jazda = 0;
}

void kalibracja(void)
{
	uint16_t min = adc[1], max = adc[1];
	uint8_t i;
	for(i = 1; i <= 8; i++)
	{
		if(adc[i] < min) min = adc[i];
		else if(adc[i] > max) max = adc[i];
	}
	prog_kalib = min+(max-min)/2;
}

void odczyt_czujnikow(void)
{
	uint8_t i;
	for(i = 1; i <= 8; i++)
	{
		if(adc[i] > prog_kalib + HIST)
			czujniki[i-1] = 1;
		else if(adc[i] < prog_kalib - HIST)
			czujniki[i-1] = 0;
	}
}

void zapisz_nastawy(void)
{
	uint8_t write[16] = {kp, kd, (vzad>>8), (vzad&0xFF), wagi[0], wagi[1], wagi[2], wagi[3], wagi[4], wagi[5], wagi[6], wagi[7]};
	HAL_I2C_Mem_Write_IT(&hi2c1, EEPROM_ADDR, 0x00, 16, write, 16);
	//HAL_Delay(50);
}

void wczytaj_nastawy(void)
{
	uint8_t read[16];
	HAL_I2C_Mem_Read_IT(&hi2c1, EEPROM_ADDR, 0, 16, read, 16);
	HAL_Delay(50);
	kp = read[0];
	kd = read[1];
	vzad = (read[2]<<8);
	vzad |= read[3];
	wagi[0] = read[4];
	wagi[1] = read[5];
	wagi[2] = read[6];
	wagi[3] = read[7];
	wagi[4] = read[8];
	wagi[5] = read[9];
	wagi[6] = read[10];
	wagi[7] = read[11];

}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	static uint8_t cnt = 0;
	if(htim->Instance == TIM7) // Timer co 10ms
	{
		if(czyszczenie) czyszczenie--;
		if(sterowanie) sterowanie--;
		if(bat_stan) bat_stan--;
		if(jazda) jazda_flag = 1;
		keys_flag = 1;
		if(++cnt == 5) // co 50ms
		{
			if(++police_light == 15) police_light = 0;
			test_flag = 1;
			cnt = 0;
		}
	}
	if(htim->Instance == TIM6) // Timer co 8ms
	{
		if(jazda) jazda_flag = 1;
	}
}

void rx_bt_ready(void)
{
	char cmd;
	int _1, _2, _3, _4, _5, _6, _7;
	sscanf((const char*)rx_buff, "%c:%d:%d:%d:%d:%d:%d:%d", &cmd, &_1, &_2, &_3, &_4, &_5, &_6, &_7);
	if(cmd == 's')
	{
		if(!jazda)
		{
			kalibracja();
			jazda = 1;
		}
	}
	else if(cmd == 'c')
	{
		sterowanie = 15;
		silniki(_1, _2);
	}
	else if(cmd == 't')
	{
		if(!test)
		{
			kalibracja();
			test = 1;
		}
	}
	else if(cmd == 'w')
	{
		kp = _1;
		kd = _2;
		vzad = _3;
		wagi[0] = -_4;
		wagi[1] = -_5;
		wagi[2] = -_6;
		wagi[3] = -_7;
		wagi[4] = _7;
		wagi[5] = _6;
		wagi[6] = _5;
		wagi[7] = _4;
		zapisz_nastawy();
		HAL_UART_Transmit_IT(&huart4, (uint8_t*) "Ok.\r\n", 4);
	}
	else if(cmd == 'g')
	{
		uint8_t len = sprintf((char*)tx_buff, "%3d:%3d:%4d:%2d:%2d:%2d:%2d\n", kp, kd, vzad, wagi[7], wagi[6], wagi[5], wagi[4]);
		HAL_UART_Transmit_IT(&huart4, (uint8_t*)tx_buff, len);
	}
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	connected = 1;
	if(jazda)
		jazda = 0;
	else if(test)
		test = 0;
	else
	{
		if(Received == '\n' || rx_size >= 32)
		{
			rx_bt_ready();
			rx_size = 0;
			memset(rx_buff, 0, 32);
		}
		else
			rx_buff[rx_size++] = Received;
	}

	HAL_UART_Receive_IT(&huart4, &Received, 1);
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler */
  /* User can add his own implementation to report the HAL error return state */
  while(1) 
  {
  }
  /* USER CODE END Error_Handler */ 
}

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
