\contentsline {section}{\numberline {1}Wst\IeC {\k e}p}{2}{section.1}
\contentsline {section}{\numberline {2}Program}{2}{section.2}
\contentsline {subsection}{\numberline {2.1}Funkcje robota}{2}{subsection.2.1}
\contentsline {subsubsection}{\numberline {2.1.1}Kalibracja czujnik\IeC {\'o}w}{2}{subsubsection.2.1.1}
\contentsline {subsubsection}{\numberline {2.1.2}Testowanie czujnik\IeC {\'o}w}{3}{subsubsection.2.1.2}
\contentsline {subsubsection}{\numberline {2.1.3}Sprawdzanie poziomu baterii}{4}{subsubsection.2.1.3}
\contentsline {subsubsection}{\numberline {2.1.4}Dodatkowe funkcje}{4}{subsubsection.2.1.4}
\contentsline {subsubsection}{\numberline {2.1.5}Jazda}{7}{subsubsection.2.1.5}
\contentsline {subsection}{\numberline {2.2}Opis algorytmu}{7}{subsection.2.2}
\contentsline {subsubsection}{\numberline {2.2.1}Cz\IeC {\l }on proporcjonalny - P}{7}{subsubsection.2.2.1}
\contentsline {subsubsection}{\numberline {2.2.2}Cz\IeC {\l }on r\IeC {\'o}\IeC {\.z}niczkuj\IeC {\k a}cy - D}{8}{subsubsection.2.2.2}
\contentsline {subsubsection}{\numberline {2.2.3}B\IeC {\l }\IeC {\k a}d ostateczny}{8}{subsubsection.2.2.3}
\contentsline {subsubsection}{\numberline {2.2.4}Zgubienie linii}{8}{subsubsection.2.2.4}
\contentsline {section}{\numberline {3}Komunikacja Bluetooth}{8}{section.3}
\contentsline {subsection}{\numberline {3.1}Przyk\IeC {\l }adowa ramka danych}{8}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Implementacja komunikacji}{9}{subsection.3.2}
\contentsline {section}{\numberline {4}Aplikacja mobilna}{9}{section.4}
\contentsline {subsection}{\numberline {4.1}Okno g\IeC {\l }\IeC {\'o}wne}{10}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Regulacja nastaw}{11}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Test czujnik\IeC {\'o}w}{12}{subsection.4.3}
\contentsline {subsection}{\numberline {4.4}Sterowanie robotem}{12}{subsection.4.4}
\contentsline {section}{\numberline {5}Problemy}{13}{section.5}
\contentsline {subsection}{\numberline {5.1}Problem z zasilaniem}{13}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Zwarcie \IeC {\'s}cie\IeC {\.z}ek}{13}{subsection.5.2}
\contentsline {section}{\numberline {6}Zako\IeC {\'n}czenie}{14}{section.6}
