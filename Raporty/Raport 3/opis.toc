\contentsline {section}{\numberline {1}Wst\IeC {\k e}p}{3}{section.1}
\contentsline {section}{\numberline {2}Opis projektu}{3}{section.2}
\contentsline {section}{\numberline {3}Efekt ko\IeC {\'n}cowy}{3}{section.3}
\contentsline {subsection}{\numberline {3.1}Robot}{3}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Aplikacja}{4}{subsection.3.2}
\contentsline {section}{\numberline {4}Narz\IeC {\k e}dzia (programy)}{4}{section.4}
\contentsline {section}{\numberline {5}Dob\IeC {\'o}r element\IeC {\'o}w elektronicznych}{4}{section.5}
\contentsline {section}{\numberline {6}Schematy ideowe p\IeC {\l }ytek}{5}{section.6}
\contentsline {subsection}{\numberline {6.1}P\IeC {\l }ytka g\IeC {\l }\IeC {\'o}wna}{6}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}P\IeC {\l }ytka z czujnikami}{9}{subsection.6.2}
\contentsline {section}{\numberline {7}Projekty oraz wykonanie p\IeC {\l }ytek}{10}{section.7}
\contentsline {subsection}{\numberline {7.1}P\IeC {\l }ytka g\IeC {\l }\IeC {\'o}wna}{10}{subsection.7.1}
\contentsline {subsection}{\numberline {7.2}P\IeC {\l }ytka z czujnikami}{11}{subsection.7.2}
\contentsline {section}{\numberline {8}Program}{12}{section.8}
\contentsline {subsection}{\numberline {8.1}Funkcje robota}{12}{subsection.8.1}
\contentsline {subsubsection}{\numberline {8.1.1}Kalibracja czujnik\IeC {\'o}w}{12}{subsubsection.8.1.1}
\contentsline {subsubsection}{\numberline {8.1.2}Testowanie czujnik\IeC {\'o}w}{12}{subsubsection.8.1.2}
\contentsline {subsubsection}{\numberline {8.1.3}Sprawdzanie poziomu baterii}{13}{subsubsection.8.1.3}
\contentsline {subsubsection}{\numberline {8.1.4}Dodatkowe funkcje}{14}{subsubsection.8.1.4}
\contentsline {subsubsection}{\numberline {8.1.5}Jazda}{17}{subsubsection.8.1.5}
\contentsline {subsection}{\numberline {8.2}Opis algorytmu}{17}{subsection.8.2}
\contentsline {subsubsection}{\numberline {8.2.1}Cz\IeC {\l }on proporcjonalny - P}{17}{subsubsection.8.2.1}
\contentsline {subsubsection}{\numberline {8.2.2}Cz\IeC {\l }on r\IeC {\'o}\IeC {\.z}niczkuj\IeC {\k a}cy - D}{18}{subsubsection.8.2.2}
\contentsline {subsubsection}{\numberline {8.2.3}B\IeC {\l }\IeC {\k a}d ostateczny}{18}{subsubsection.8.2.3}
\contentsline {subsubsection}{\numberline {8.2.4}Zgubienie linii}{18}{subsubsection.8.2.4}
\contentsline {section}{\numberline {9}Komunikacja Bluetooth}{18}{section.9}
\contentsline {subsection}{\numberline {9.1}Przyk\IeC {\l }adowa ramka danych}{18}{subsection.9.1}
\contentsline {subsection}{\numberline {9.2}Implementacja komunikacji}{19}{subsection.9.2}
\contentsline {section}{\numberline {10}Aplikacja mobilna}{19}{section.10}
\contentsline {subsection}{\numberline {10.1}Okno g\IeC {\l }\IeC {\'o}wne}{20}{subsection.10.1}
\contentsline {subsection}{\numberline {10.2}Regulacja nastaw}{22}{subsection.10.2}
\contentsline {subsection}{\numberline {10.3}Test czujnik\IeC {\'o}w}{22}{subsection.10.3}
\contentsline {subsection}{\numberline {10.4}Sterowanie robotem}{23}{subsection.10.4}
\contentsline {section}{\numberline {11}Problemy}{23}{section.11}
\contentsline {subsection}{\numberline {11.1}Problem z zasilaniem}{23}{subsection.11.1}
\contentsline {subsection}{\numberline {11.2}Zwarcie \IeC {\'s}cie\IeC {\.z}ek}{24}{subsection.11.2}
\contentsline {section}{\numberline {12}Harmonogram realizacji}{24}{section.12}
\contentsline {section}{\numberline {13}Diagram Gantta}{24}{section.13}
\contentsline {section}{\numberline {14}Kamienie milowe}{24}{section.14}
\contentsline {section}{\numberline {15}Zako\IeC {\'n}czenie}{25}{section.15}
